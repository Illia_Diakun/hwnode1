const express = require('express')
const app = express()
const  fs = require('fs');
const bodyParser = require('body-parser')
const urlencodedParser = bodyParser.urlencoded({ extended: false, })

app.post('/api/files', urlencodedParser, function (request, response) {
    console.log(request.body)
    try {
    if(!request.body.content) {
        response.status(400).send( {
            "message": "Please specify 'content' parameter"
        })
        return;
    }
    if(!request.body.filename) {
            response.status(400).send( {
                "message": "Please specify 'content' parameter"
            })
            return;
        }

    fs.appendFile(`./HW1/files/${request.body.filename}`, request.body.content, () => {
            response.status(200).send({
                "message": "File created successfully",
            })
        });
    } catch (er) {
        console.log(er);

        response.status(500).send({
            "message": "Server error"
        })
    }
})


 app.get('/api/files', function (request, response) {
     try {
         fs.readdir("./HW1/files/", (err, files) => {
            response.status(200).send({
                "message": "Success",
                 "files": files
             })
         });
    } catch (er) {
         response.status(500).send({
            "message": "Server error"
        })
    }

 })



app.get('/api/files/:filename', function (request, response) {
    try {
        const filename = request.params.filename
        const path = `./HW1/files/${filename}`;

        let content;
        try {
             content = fs.readFileSync(path, {encoding: "utf-8"})
        } catch (e) {
            response.status(400).send({
                "message":`No file with '${filename}' filename found`
            })
        }
        const uploadedDate = fs.statSync(path).ctime;
        response.status(200).send({
            message: "Success",
            filename,
            content,
            extension: filename.split('.' ).pop(),
            uploadedDate
        })
    } catch (er) {
        response.status(500).send({
            "message": "Server error"
        })
    }

})

app.listen(8080)
